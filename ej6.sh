read -p "Introduce cantidad de litros de agua consumidos: " litros

primeros=$((50*20));
segundos=$(($primeros+30))

if [ $litros -le 50 ]; then
	coste=$(($litros * 20))
elif [ $litros -le 200 ]; then
	coste=`echo "scale=2; $primeros+(($litros-50)*0.2)" | bc`;
else
	coste=`echo "scale=2; $segundos+(($litros-200)*0.1)" | bc`;
fi

echo "El coste por $litros litros es de $coste"
