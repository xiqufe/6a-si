read -p "Introduce un numero: " numero
sumvalores=$numero;
valoresintroducidos=0;

while [ $numero -gt 0 ];
do
	read -p "Introduce un numero: " numero
	sumvalores=`expr $sumvalores + $numero`;
	valoresintroducidos=`expr $valoresintroducidos + 1 `;
done

echo "La suma del total de los valores introducidos es de $sumvalores"
echo "El valor medio es `expr $sumvalores / $valoresintroducidos`"
