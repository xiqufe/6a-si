read -p "Introduce un dia: " dia

while [ $dia -lt 0 -o $dia -gt 30 ]
do
	read -p "Introduce un dia: " dia
done

secuencia=0
continuar=true

while [ $continuar ]; do

	for dias in lunes martes miercoles jueves viernes sabado domingo; do

		secuencia=$((secuencia+1))
		if [ $dia = $secuencia ]
		then
			echo "$dias"
			exit
		fi
	done
	
	continuar=false

done
