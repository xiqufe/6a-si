read -p "Introduce un numero: " numero

while [ $numero -lt 1 ]
do
	read -p "Introduce un numero mayor a 0: " numero
done

if [ `expr $numero % 2` -eq 0 ]; then
	echo "El numero $numero es par."
else
	echo "El numero $numero es impar."
fi
