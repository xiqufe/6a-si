read -p "Introduce una nota: " nota

while [ $nota -lt 0 -o $nota -gt 10 ]
do
	read -p "Introduce una nota valida: " nota
done

if [ $nota -lt 5 ]; then
	echo "Suspenso"
elif [ $nota -eq 5 ]; then
	echo "Aprobado"
elif [ $nota -eq 6 ]; then
	echo "Bien"
elif [ $nota -lt 9 ]; then
	echo "Notable"
else
	echo "Sobresaliente"
fi
